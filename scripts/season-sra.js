Hooks.once('init', function () {
  // Définition des attributs par défaut
  CONFIG.SeasonSRA = {
    attributes: {
      strength: { label: 'Force', value: 1 },
      agility: { label: 'Agilité', value: 1 },
      willpower: { label: 'Volonté', value: 1 },
      logic: { label: 'Logique', value: 1 },
      charisma: { label: 'Charisme', value: 1 },
      luck: { label: 'Chance', value: 1 },
    },
    skills: {
      acrobatics: { label: 'Acrobaties', value: 0 },
      firearms: { label: 'Armes à feu', value: 0 },
      projectiles: { label: 'Armes à projectiles', value: 0 },
      vehicleWeapons: { label: 'Armes de véhicules', value: 0 },
      heavyWeapons: { label: 'Armes lourdes', value: 0 },
      meleeCombat: { label: 'Corps à Corps', value: 0 },
      stealth: { label: 'Furtivité', value: 0 },
      diverseVehicles: { label: 'Véhicules divers', value: 0 },
      landVehicles: { label: 'Véhicules terrestres', value: 0 },
      // Ajoute d'autres compétences selon tes besoins
    },
    perks: {
      // Atouts
    },
  };

  // ... Autres configurations

  // Fonction pour créer un personnage avec des valeurs par défaut
  function createSeasonSRACharacter() {
    let characterData = {
      attributes: duplicate(CONFIG.SeasonSRA.attributes),
      skills: duplicate(CONFIG.SeasonSRA.skills),
      perks: duplicate(CONFIG.SeasonSRA.perks),
    };
    return characterData;
  }

  // ... Autres fonctions et configurations

  // Exemple d'utilisation pour créer un personnage
  let newCharacter = createSeasonSRACharacter();
  console.log(newCharacter);
});
